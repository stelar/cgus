Conditions d'utilisation de Google Drive
Dernière modification : 10 décembre 2018
Effective Date: January 22, 2019
1. Introduction

Merci d'utiliser Google Drive. Google Drive est un service fourni par Google LLC ("Google" ou "nous"), société située au 1600 Amphitheatre Parkway, Mountain View, CA 94043, États-Unis. Si vous résidez dans l'Espace économique européen ou en Suisse, Google Drive est fourni par Google Ireland Limited ("Google" ou "nous"), une entreprise enregistrée en Irlande et soumise au droit de ce pays (numéro d'enregistrement : 368047), et dont l'adresse est Gordon House, Barrow Street, Dublin 4, Irlande. Les conditions d'utilisation de Google Drive (désignées par le terme "Conditions") concernent votre utilisation de Google Drive, votre accès à Google Drive ainsi que vos contenus dans Google Drive. Nos Règles de confidentialité expliquent comment nous collectons et utilisons vos données, tandis que le Règlement du programme passe en revue vos responsabilités quant à l'utilisation de notre Service.

Pour utiliser Google Drive, vous devez accepter ces Conditions. Nous vous invitons à les lire attentivement. Si vous ne comprenez pas les présentes Conditions ou si vous ne les acceptez pas, même partiellement, vous ne devez pas utiliser Google Drive.

2. Votre utilisation de Google Drive

Limites d'âge. Pour pouvoir utiliser Google Drive, vous devez avoir au moins 13 ans. Si vous avez 13 ans ou plus, mais moins de 18 ans, vous devez obtenir l'autorisation d'un parent ou d'un tuteur légal pour utiliser Google Drive et accepter les Conditions.

Usage personnel. En acceptant les présentes Conditions, vous vous engagez à ne pas utiliser Google Drive dans un cadre professionnel. Le service Drive est réservé à un usage personnel non commercial. Nous recommandons aux entreprises d'utiliser G Suite.

Votre compte Google. Pour pouvoir utiliser Google Drive, vous devez disposer d'un compte Google. Pour protéger votre compte Google, préservez la confidentialité de votre mot de passe. Vous êtes responsable de l'activité exercée dans votre compte Google ou par le biais de celui-ci. Veillez à ne pas réutiliser le mot de passe associé à votre compte Google dans des applications tierces. Si vous pensez que votre mot de passe ou votre compte Google a été utilisé sans votre autorisation, suivez ces instructions.

Votre comportement. Vous ne devez pas utiliser Google Drive à mauvais escient. Votre utilisation de Google Drive doit respecter la législation en vigueur, y compris les lois et réglementations applicables relatives au contrôle des exportations et réexportations. Vous êtes responsable de votre comportement et des contenus que vous stockez dans Google Drive, et vous devez en outre respecter le Règlement du programme. Nous sommes susceptibles d'examiner votre comportement et les contenus que vous stockez dans Google Drive pour vérifier que vous respectez les présentes Conditions ainsi que le Règlement du programme.

Google Drive est disponible sur les appareils mobiles. Ne l'utilisez pas d'une manière susceptible de vous distraire et de vous empêcher de respecter le Code de la route et les règles de sécurité en matière de conduite.

Vos contenus. Google Drive vous offre la possibilité d'importer, de mettre en ligne, de stocker, d'envoyer et de recevoir des contenus. Vous conservez tous vos droits de propriété intellectuelle sur ces contenus. Vous restez le propriétaire de ce qui vous appartient.

Lorsque vous importez, mettez en ligne, stockez, envoyez ou recevez des contenus dans ou par le biais de Google Drive, vous accordez à Google une licence mondiale d'utilisation, d'hébergement, de stockage, de reproduction, de modification, de création d'œuvres dérivées (traductions, adaptations ou autres modifications destinées à améliorer la compatibilité de vos contenus avec nos services), de communication, de publication, de représentation publique, d'affichage public ou de distribution publique desdits contenus. Les droits que vous accordez dans le cadre de cette licence sont limités à l'exploitation, à la promotion et à l'amélioration de nos services, et au développement de nouveaux services. Ces droits restent valables même si vous cessez d'utiliser nos services, sauf si vous supprimez vos contenus. Assurez-vous de disposer des droits qui vous permettent de nous accorder cette licence concernant les contenus que vous importez dans Google Drive.

Les paramètres de partage de Google Drive vous permettent de définir dans quelle mesure les autres utilisateurs peuvent utiliser vos contenus stockés dans Google Drive. Par défaut, vous contrôlez l'ensemble des contenus que vous créez ou importez dans Google Drive. Vous pouvez partager vos contenus et transférer à d'autres utilisateurs la possibilité de contrôler ces contenus.

Nos systèmes automatisés analysent vos contenus pour vous proposer des fonctionnalités pertinentes (résultats de recherche personnalisés, par exemple) et pour détecter le spam et les logiciels malveillants. Cette analyse a lieu lors de la réception, du partage et de l'importation des contenus, mais aussi tout au long de la période de stockage. Pour en savoir plus sur la façon dont Google utilise et stocke les contenus, consultez nos Règles de confidentialité. Les commentaires ou suggestions dont vous nous faites part concernant Google Drive peuvent être utilisés par Google sans obligation envers vous.

Annonces. Dans le cadre de votre utilisation de Google Drive, nous sommes susceptibles de vous envoyer des annonces relatives au service, des messages d'ordre administratif ainsi que d'autres informations. Vous pouvez choisir de ne plus recevoir certains de ces messages.

Nos services Google Drive. Le fait d'utiliser Google Drive ne vous confère aucun droit de propriété intellectuelle relatif à Google Drive ou aux contenus auxquels vous accédez. Vous ne devez pas utiliser les contenus issus de Google Drive sans l'autorisation du propriétaire, sauf si la loi vous y autorise. Les présentes Conditions ne vous accordent pas le droit d'utiliser les marques et autres logos figurant dans Google Drive. Vous ne devez pas supprimer, masquer ni altérer d'aucune façon que ce soit les mentions légales figurant dans Google Drive ou relatives à Google Drive.

3. Protection de la vie privée

Les Règles de confidentialité de Google décrivent comment nous traitons vos données personnelles et protégeons votre vie privée lorsque vous utilisez Google Drive. En utilisant Google Drive, vous acceptez que Google utilise ces données conformément aux règles de confidentialité.

4. Protection des droits d'auteur

Nous répondons aux notifications d'atteinte supposée aux droits d'auteur et désactivons les comptes des utilisateurs ayant plusieurs fois porté atteinte à ces droits, conformément à la procédure établie par la loi américaine Digital Millennium Copyright Act.

Nous fournissons aux titulaires de droits d'auteur les informations nécessaires pour les aider à gérer leur propriété intellectuelle en ligne. Si vous pensez qu'un utilisateur porte atteinte à vos droits d'auteur et que vous souhaitez nous en avertir, veuillez consulter notre Centre d'aide. Vous y trouverez des instructions concernant l'envoi de notifications pour atteinte aux droits d'auteur, ainsi que des informations sur la façon dont Google traite ce type de notifications.

5. Règlement du programme

Nous pouvons examiner vos contenus pour vérifier qu'ils ne sont pas illégaux et qu'ils n'enfreignent pas le Règlement du programme. Nous sommes également susceptibles de supprimer ou de refuser d'afficher les contenus dont nous pouvons raisonnablement penser qu'ils enfreignent nos règles ou la loi. Toutefois, le fait que nous nous réservions ce droit ne signifie pas nécessairement que nous vérifions les contenus.

6. À propos des logiciels utilisés dans nos Services

Logiciel client. Google Drive intègre un logiciel client téléchargeable ("Logiciel"). Ce Logiciel peut se mettre à jour automatiquement sur votre appareil lorsqu'une nouvelle version ou une nouvelle fonctionnalité est disponible. Google vous concède une licence personnelle, libre de droit, non cessible, non exclusive et valable dans le monde entier qui vous autorise à utiliser le Logiciel fourni par Google dans le cadre de Google Drive. Cette licence a pour seul but de vous permettre d'utiliser Google Drive et de profiter de ses avantages, dans les conditions prévues par Google et dans le respect des présentes Conditions. Vous n'êtes pas autorisé à copier, modifier, distribuer, vendre ni louer une partie ou la totalité de Google Drive ou du logiciel qui en fait partie. De même, vous n'êtes pas autorisé à décompiler ni à tenter d'extraire le code source de ce logiciel, sauf si ces restrictions sont interdites par la loi ou si vous avez obtenu notre autorisation écrite.

Logiciels Open Source. Nous sommes attachés à l'utilisation des logiciels Open Source. Certains des logiciels utilisés dans Google Drive peuvent être concédés sous licence Open Source mise à votre disposition. Ladite licence Open Source peut contenir des dispositions qui prévalent expressément sur certaines de ces Conditions.

7. Modification et résiliation de Google Drive

Modifications apportées à Google Drive. Google modifie et améliore Google Drive en permanence. Nous sommes donc susceptibles d'intégrer des améliorations liées aux performances ou à la sécurité, de modifier certaines caractéristiques ou fonctionnalités, et d'apporter des modifications pour nous mettre en conformité avec la loi ou pour empêcher des activités illégales survenant sur nos systèmes ou une utilisation abusive de ces derniers. Pour recevoir des informations sur Google Drive, cliquez ici. Chaque fois que nous apportons des modifications substantielles dont nous pensons raisonnablement qu'elles peuvent avoir une incidence négative sur votre utilisation de Google Drive, vous en serez informé. Cependant, il peut arriver, dans certains cas, que nous soyons amenés à apporter des modifications à Google Drive sans pouvoir vous prévenir. Ces situations se limitent aux cas où nous devons prendre des mesures pour assurer la sécurité et le bon fonctionnement du service, pour empêcher des abus ou pour nous mettre en conformité avec la législation.

Suspension et résiliation. Bien que nous soyons heureux de vous compter parmi nous, vous pouvez cesser d'utiliser Google Drive à tout moment. Nous sommes susceptibles d'interrompre ou de désactiver définitivement votre accès à Google Drive si vous enfreignez de façon significative ou répétée nos Conditions ou le Règlement du programme. En cas de suspension ou de désactivation de votre accès à Google Drive, vous en serez informé à l'avance. Nous pouvons toutefois être amenés à suspendre ou à désactiver votre accès à Google Drive sans préavis si vous utilisez ce service d'une façon susceptible de mettre en jeu notre responsabilité légale ou d'empêcher d'autres personnes d'utiliser Google Drive.

Abandon de Google Drive. Si nous décidons d'abandonner définitivement Google Drive, vous en serez informé au moins 60 jours à l'avance. Pendant cette période de préavis, vous aurez la possibilité d'extraire vos fichiers de Google Drive. Au terme de cette période de 60 jours, vous n'aurez plus accès à vos fichiers. Nous partons du principe que vous êtes le propriétaire de vos fichiers et qu'il est important que vous en conserviez l'accès. Pour savoir comment télécharger vos fichiers, consultez notre page d'assistance.

8. Achat d'espace de stockage supplémentaire et paiements

Espace de stockage gratuit. Google vous offre 15 Go d'espace de stockage en ligne gratuit (dont l'utilisation est soumise aux présentes Conditions), qui peuvent être utilisés avec Google Drive, Gmail et Google Photos.

Achat d'espace de stockage supplémentaire. Vous pouvez également acheter de l'espace de stockage supplémentaire ("Forfait de stockage payant"), en fonction de vos besoins. Nous facturerons automatiquement ce service à compter de la date à laquelle vous passerez à un Forfait de stockage payant, puis à chaque renouvellement périodique du service, jusqu'à résiliation. Pour pouvoir acheter un Forfait de stockage payant, vous devez accepter les conditions de paiement spécifiées dans les Conditions d'utilisation de Google Payments. Si vous n'avez pas de compte Google Payments, vous pouvez en créer un en accédant à ce lien, où vous trouverez également d'autres informations sur le service. Les Conditions d'utilisation et l'Avis de confidentialité de Google Payments s'appliquent également à chaque fois que vous souhaitez acheter un Forfait de stockage payant via un compte Google Payments. Veillez à lire attentivement ces conditions d'utilisation avant d'effectuer un achat.

Résiliation. Votre Forfait de stockage payant demeure en vigueur tant que vous ne le résiliez pas, que vous ne repassez pas à un forfait inférieur ou qu'il n'y est pas mis fin conformément aux présentes Conditions. Vous pouvez résilier ou passer à un forfait inférieur à tout moment à partir des paramètres de stockage de votre compte Google Drive. La résiliation ou le passage à un forfait inférieur s'appliquera à la première période de facturation suivant la date d'expiration du service en cours. Si vous ne réglez pas à temps les frais liés à votre Forfait de stockage payant, nous nous réservons le droit de vous faire repasser à un forfait inférieur et de réduire votre espace de stockage. Vous bénéficierez alors d'une quantité d'espace équivalant à celle d'un compte gratuit. La procédure de résiliation et de remboursement du Forfait de stockage payant est expliquée dans notre article Modalités d'achat, de résiliation et de remboursement.

Modifications apportées aux forfaits et aux tarifs. Nous sommes susceptibles de modifier les forfaits de stockage et les tarifs en vigueur, mais vous serez informé à l'avance de tels changements. Ceux-ci ne prendront effet qu'après la date d'expiration du service en cours, au moment où le paiement suivant est dû, après le préavis. En cas d'augmentation des tarifs ou de réduction de l'espace de stockage proposé, vous en serez informé au moins 30 jours avant facturation. Si le délai de préavis est inférieur à 30 jours, le changement ne s'appliquera que lorsque le paiement suivant sera dû. Si vous ne souhaitez pas conserver le forfait de stockage modifié ou régler le nouveau montant, vous pouvez résilier votre Forfait de stockage payant ou passer à un forfait inférieur à tout moment à partir des paramètres de stockage de votre compte Google Drive. La résiliation ou le passage à un forfait inférieur s'appliquera à la première période de facturation suivant la date d'expiration du service en cours. Vos fichiers resteront disponibles ou nous vous offrirons la possibilité de les extraire de Google Drive.

9. Garanties et clauses de non-responsabilité

Notre offre Google Drive est soumise à une obligation de moyens, et nous espérons que vous apprécierez ce produit. Google Drive fait cependant l'objet d'une limitation de garantie. Sauf mention explicite du contraire, nous ne faisons aucune promesse concernant des fonctionnalités spécifiques disponibles via Google Drive, ni concernant sa fiabilité, sa disponibilité ou son adéquation avec vos besoins.

10. Responsabilité pour Google Drive

Google ainsi que ses fournisseurs et ses distributeurs ne peuvent être tenus responsables :

(a) des pertes qui ne sont pas dues à un manquement de notre part aux présentes Conditions ;

(b) des pertes et dommages qui, au moment de la signature du contrat concerné avec vous, ne faisaient pas partie des conséquences raisonnablement prévisibles d'un manquement de Google aux présentes Conditions ;

(c) des pertes liées à vos activités, y compris d'un manque à gagner ou d'une perte de chiffre d'affaires, d'opportunités commerciales ou de données.

La responsabilité totale de Google, ainsi que de ses fournisseurs et distributeurs, pour toute réclamation dans le cadre des présentes conditions, notamment pour les garanties implicites, est limitée au montant que vous nous avez réglé pour utiliser les services (ou, si l'objet de la réclamation est un service gratuit, elle se limite à vous fournir de nouveau l'accès aux services).

Aucune disposition des présentes Conditions ne saurait exclure ou limiter la responsabilité de Google ainsi que de ses fournisseurs et distributeurs en cas de décès ou de dommage corporel, de fraude ou de représentation frauduleuse, ni d'une quelconque responsabilité dont le droit interdit l'exclusion.

11. Droit régissant les présentes Conditions

Si vous vivez en dehors de l'Espace européen ou de la Suisse, les éventuels litiges liés aux présentes conditions d'utilisation ou à Google Drive seront régis par les lois de l'État de Californie, États-Unis, à l'exclusion des règles de conflit de lois de cet État. Toute action en justice liée aux présentes conditions d'utilisation ou à Google Drive relèvera exclusivement de la juridiction des tribunaux fédéraux ou des tribunaux d'État du comté de Santa Clara, Californie, États-Unis. Google et vous-même acceptez par les présentes de vous soumettre à la compétence de ces tribunaux.

Si vous vivez dans l'Espace européen ou en Suisse, les éventuels litiges liés aux présentes conditions d'utilisation ou à Google Drive seront régis par le droit et les tribunaux de votre pays de résidence, et vous pourrez intenter une poursuite judiciaire auprès de vos tribunaux locaux. Les litiges peuvent faire l'objet d'une tentative de règlement en ligne par le biais de la plate-forme de règlement en ligne des litiges de la Commission européenne.

12. À propos des présentes Conditions

Nous sommes susceptibles de modifier les présentes Conditions ou toute autre condition supplémentaire s'appliquant à Google Drive, par exemple, pour prendre en compte des modifications apportées à Google Drive ou à la loi, ou des changements au niveau des usages, ou des règles politiques ou économiques, ou encore en réponse à des consignes émanant des organismes de réglementation ou d'institutions propres aux secteurs d'activité concernés. Nous pouvons également être amenés à modifier ces Conditions pour respecter nos obligations. Nous vous recommandons de consulter régulièrement les Conditions. Les modifications apportées aux présentes Conditions seront signalées sur cette page. Nous publierons un avis de modification des conditions d'utilisation supplémentaires ("Conditions supplémentaires") dans Google Drive, et vous serez informé à l'avance de toute modification significative des Conditions. Les modifications ne s'appliqueront pas de façon rétroactive et entreront en vigueur au moins 14 jours après la publication ou le préavis. Toutefois, les modifications propres à une nouvelle caractéristique ou fonctionnalité ("Nouveaux services"), ou mises en place pour des raisons légales, prendront effet immédiatement. Si vous n'acceptez pas les modifications apportées aux Conditions d'un Nouveau service, vous devez cesser toute utilisation de ce Nouveau service (pour en savoir plus, consultez le paragraphe "Résiliation" ci-dessus).

En cas de conflit entre les présentes Conditions et des Conditions supplémentaires, ces dernières prévalent.

Les présentes Conditions régissent votre relation avec Google. Elles ne créent pas de droit pour des tiers bénéficiaires.

Si vous ne respectez pas les présentes Conditions, et que nous ne prenons pas immédiatement de mesure à ce sujet, cela ne signifie pas pour autant que nous renonçons à nos droits (par exemple, à prendre des mesures ultérieurement).

S'il s'avère qu'une condition d'utilisation particulière n'est pas applicable, cela sera sans incidence sur les autres conditions d'utilisation.

Pour toute information sur la procédure à suivre pour contacter Google, veuillez consulter la page de contact.
